#!/usr/bin/env python
import setuptools
from datetimeutils import __version__


readme = None
try:
    with open('README.md') as f:
        readme = f.read()
except Exception as ex:
    pass


license = None
try:
    with open('LICENSE') as f:
        license = f.read()
except Exception as ex:
    pass


setuptools.setup(
    name='datetimeutils',
    version=__version__,
    classifiers=[
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.8',
        'Programming Language :: Python :: 3.9',
        'Programming Language :: Python :: 3.10',
        'Programming Language :: Python :: 3.11',
        'Programming Language :: Python :: 3.12',
    ],
    description='Utilities for date and/or time objects and values.',
    long_description=readme,
    author='Thordur Matthiasson',
    author_email='thordurm@ccpgames.com',
    url='https://gitlab.com/ccptools/datetimeutils',
    packages=setuptools.find_packages(exclude=('tests',)),
    install_requires=[]
)
