"""This is the main API module of datetimeutils.

The intended use case is to go:
```python
from datetimeutils import dtu
```

And then use `dtu` in your code as it serves as the "official" API of the
package.
"""
from datetimeutils.structs import *

from datetimeutils.shortcuts import *

from datetimeutils.casting import *
from datetimeutils.formatting import *
from datetimeutils.parsers import *

from datetimeutils import __version__ as VERSION  # noqa
