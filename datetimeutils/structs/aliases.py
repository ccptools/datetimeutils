__all__ = [
    'Date',
    'Time',
    'Datetime',
    'TimeDelta',
    'TzInfo',
    'TimeZone',

    'T_DATETIME_VALUE',
    'T_DATE_VALUE',
    'T_TIME_VALUE',
    'T_TEMPORAL_VALUE',
    'T_NUMBER',
]
from ._base import *

# Aliases to "standard" PEP 8 class names. This is to increase readability and
# reduce confusion related to the fact that the most commonly used class/object
# shared the name of its own module (i.e. `datetime.datetime`)
Date = datetime.date
Time = datetime.time
Datetime = datetime.datetime
TimeDelta = datetime.timedelta
TzInfo = datetime.tzinfo
TimeZone = datetime.timezone

# Any datetime value
T_DATETIME_VALUE = Union[datetime.datetime, datetime.date, datetime.time]

# Containing a date
T_DATE_VALUE = Union[datetime.datetime, datetime.date]

# Containing time
T_TIME_VALUE = Union[datetime.datetime, datetime.time]

# Any type that can possibly contain a date and/or time value
T_TEMPORAL_VALUE = Union[T_DATETIME_VALUE, float, int, str, bytes]

# Number
T_NUMBER = Union[int, float]
